%include kuklinux-base.ks

## crystal
repo --name=crystal --baseurl=https://download.copr.fedorainfracloud.org/results/zawertun/crystal/fedora-$releasever-$basearch/

# budgie
repo --name=budgie --baseurl=https://download.copr.fedorainfracloud.org/results/alunux/budgie-desktop-git/fedora-$releasever-$basearch/


%packages

# dev
@development-tools
crystal
shards

# desktop environments
arc-icon-theme
budgie-brightness-control-applet
budgie-desktop
budgie-haste-applet
budgie-pixel-saver-applet
gnome-software
gnome-terminal
moka-icon-theme

# editors
emacs
emacs-auto-complete
emacs-terminal
gedit
gedit-plugins
gedit-plugins-data

# internet
firefox
epiphany

%end


%post

# add repos
## budgie
cat << 'EOF' > /etc/yum.repos.d/_copr:copr.fedorainfracloud.org:alunux:budgie-desktop-git.repo
[copr:copr.fedorainfracloud.org:alunux:budgie-desktop-git]
name=Copr repo for budgie-desktop-git owned by alunux
baseurl=https://download.copr.fedorainfracloud.org/results/alunux/budgie-desktop-git/fedora-$releasever-$basearch/
type=rpm-md
skip_if_unavailable=True
gpgcheck=1
gpgkey=https://download.copr.fedorainfracloud.org/results/alunux/budgie-desktop-git/pubkey.gpg
repo_gpgcheck=0
enabled=1
enabled_metadata=1

EOF

## crystal
cat << 'EOF' > /etc/yum.repos.d/_copr:copr.fedorainfracloud.org:zawertun:crystal.repo
[copr:copr.fedorainfracloud.org:zawertun:crystal]
name=Copr repo for crystal owned by zawertun
baseurl=https://download.copr.fedorainfracloud.org/results/zawertun/crystal/fedora-$releasever-$basearch/
type=rpm-md
skip_if_unavailable=True
gpgcheck=1
gpgkey=https://download.copr.fedorainfracloud.org/results/zawertun/crystal/pubkey.gpg
repo_gpgcheck=0
enabled=1
enabled_metadata=1

EOF

# set background
## get it
curl https://downloads.woralelandia.com/bg/kgl.png > /usr/share/backgrounds/default.png

## set dconf
cat << EOF > /etc/dconf/db/local.d/00-background
[org/gnome/desktop/background]
picture-uri='file:///usr/share/backgrounds/default.png'
picture-options='stretched'

EOF

## add locks
cat << EOF > /etc/dconf/db/local.d/locks/background
# List the keys used to configure the desktop background
/org/gnome/desktop/background/picture-uri
/org/gnome/desktop/background/picture-options

EOF

# budgie
cat << EOF > /etc/dconf/db/local.d/00-budgie
[org/gnome/desktop/interface]
gtk-theme='Adapta-Nokto-Eta'
icon-theme='Moka'
EOF

# update dconf
dconf update

%end
