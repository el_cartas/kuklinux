===============
Kuk GNU & Linux
===============
---------------------------------
Distribución Kuk basada en Fedora
---------------------------------

Descripción
===========
Esta distro es solo una re-edición de Fedora. Nada especial.

El repositorio contiene lo necesario para construirla en tu PC. Solo necesitas usar el Makefile contenido.

Características
===============
* Usa budgie-desktop como escritorio primario
* Tiene instalado:
    - crystal
    - vim
    - gnome-terminal

Dependencias
============
En Fedora:

* @virtualization
* git
* lorax
* mock
* wget

Instrucciones
=============
Primero, instala todo.

Luego, hazte miembro de los grupos necesarios:

.. code-block:: sh

    # como root
    su -

    # modifica tu usuario
    usermod -aG mock,kvm,qemu,libvirt renich

    # en vez de hacer ésto, puedes hacer logout y login...
    newgrp mock
    newgrp kvm
    newgrp qemu
    newgrp libvirt

Luego, para construir el iso, solo:

.. code-block:: sh

    make update-boot
    make build

.. note::
    Hay cosas que requieren sudo. De repente, te pedirá tu contraseña.

Colaboradores
=============
* Renich Bon Ćirić <renich@woralelandia.com>

Referencias
===========
* https://weldr.io/lorax/livemedia-creator.html

